# Supermicro IPMI Exporter for Prometheus
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black) [![pipeline status](https://gitlab.com/trojsten/infrastructure/supermicro-ipmi-exporter/badges/master/pipeline.svg)](https://gitlab.com/trojsten/infrastructure/supermicro-ipmi-exporter/commits/master)


## Installation
ipmi-exporter is a Python module so you can install it easily via pip:
```
$ pip install git+https://gitlab.com/trojsten/infrastructure/supermicro-ipmi-exporter
```

## Usage
Run `ipmi-exporter` with a specified config file. You can find an example config
[here](https://gitlab.com/trojsten/infrastructure/supermicro-ipmi-exporter/blob/master/config.ini).
```
ipmi-exporter -c /path/to/config.ini
```
Now visit http://localhost:9920/metrics?target=47.47.47.47 where `47.47.47.47`
is the IP of the device to get metrics from.

## Build
To build a docker image simply run `make`

## Help
```
usage: ipmi-exporter [-h] -c CONFIG

Supermicro IPMI Exporter for Prometheus

Results from following SMCIPMITool subcommands will be exported:
 - ipmi power status - user privilege required
 - ipmi sensor       - user privilege required
 - pminfo            - operator privilege required (exporter will work will
                       work with user privilege but no pminfo will be scraped)

optional arguments:
  -h, --help  show this help message and exit
  -c CONFIG   path to config file

```

## Prometheus Configuration
The supermicro ipmi exporter needs to be passed the address as a parameter,
this can be done with relabeling.

Example config:
```yaml
scrape_configs:
  - job_name: ipmi-exporter
    scrape_interval: 2m
    scrape_timeout: 2m
    static_configs:
      - targets:
        - '10.0.20.1'
        - '10.0.20.2'
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: ipmi-exporter:9920
```

## Configuration
You can set logging level by setting environment variable `LOG_LEVEL` to
corresponding **numeric** [log level value](https://docs.python.org/3/library/logging.html#levels).
Default level is set to `INFO`.

## Development
You can run ipmi-exporter and a local instance of prometheus by executing:
```
$ make
$ docker-compose up
```
Then visit `localhost:9090` to browse collected metric via prometheus.

## Notice
We include the SMCIPMITool with the exporter. The reason for doing so is that
_supermicro_ removes the old version from their [website](ftp://ftp.supermicro.com/utility/SMCIPMItool/Linux/) when they upload a new one. This breaks our builds every-time
they upload a new version.

#### License
We found an Apache License in the `SMCIPMITool.jar` (in
`SMCIPMITool.jar/META-INF/LICENSE.txt`). However, there is not explicitly
written that it applies to `SMCIPMITool`. We hope it does. In case you have any
issues with this decision, please contact us. We are sure we will be able to
discuss & resolve it!
