[SlaveAddress = 78h] [Module 1]
Item                           |                          Value
----                           |                          -----
Status                         |               [STATUS OK](01h)
Input Voltage                  |                        222.5 V
Input Current                  |                         0.51 A
Main Output Voltage            |                        12.42 V
Main Output Current            |                         6.25 A
Temperature 1                  |                        29C/84F
Temperature 2                  |                            N/A
Fan 1                          |                       6592 RPM
Fan 2                          |                       8800 RPM
Main Output Power              |                           74 W
Input Power                    |                          104 W
PMBus Revision                 |                            N/A
PWS Serial Number              |                            N/A
PWS Module Number              |                   PWS-1K41P-1R
PWS Revision                   |                         REV1.0

[SlaveAddress = 7Ah] [Module 2]
Item                           |                          Value
----                           |                          -----
Status                         |                          (00h)
Input Voltage                  |                        222.5 V
Input Current                  |                         0.51 A
Main Output Voltage            |                         12.0 V
Main Output Current            |                         6.25 A
Temperature 1                  |                        29C/84F
Temperature 2                  |                            N/A
Fan 1                          |                       7008 RPM
Fan 2                          |                      10496 RPM
Main Output Power              |                           74 W
Input Power                    |                          104 W
PMBus Revision                 |                            N/A
PWS Serial Number              |                            N/A
PWS Module Number              |                   PWS-1K41P-1R
PWS Revision                   |                         REV1.0
