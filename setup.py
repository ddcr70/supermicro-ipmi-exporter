from setuptools import find_packages, setup

setup(
    name='ipmi-exporter',
    version='0.1.0',
    author='Matus Ferech',
    author_email='matus.ferech at trojsten.sk',
    description='Supermicro IPMI Exporter for Prometheus',
    url='https://gitlab.com/trojsten/infrastructure/supermicro-ipmi-exporter',
    packages=find_packages(exclude=('tests',)),
    entry_points={'console_scripts': ['ipmi-exporter=ipmi_exporter.cli:main']},
    keywords='prometheus monitoring exporter'.split(),
    install_requires=['prometheus-client==0.4.2'],
    classifiers=[
        'Environment :: Console',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
    ],
)
