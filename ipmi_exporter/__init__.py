import logging
from os import getenv

logging.basicConfig(
    level=int(getenv('LOG_LEVEL', logging.INFO)),
    format='%(asctime)s %(name)-22s %(levelname)-8s %(message)s',
)
