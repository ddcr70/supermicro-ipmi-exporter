'''Supermicro IPMI Exporter for Prometheus

Results from following SMCIPMITool subcommands will be exported:
 - ipmi power status - user privilege required
 - ipmi sensor       - user privilege required
 - pminfo            - operator privilege required (exporter will work will
                       work with user privilege but no pminfo will be scraped)
'''
import logging
from sys import stderr
from os import getenv
from configparser import ConfigParser, NoOptionError
from argparse import ArgumentParser, RawDescriptionHelpFormatter

from .server import ThreadingHTTPServer, IPMICollectorHandler


def get_args():
    parser = ArgumentParser(
        prog='ipmi-exporter',
        description=__doc__,
        formatter_class=RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        '-c',
        action='store',
        help='path to config file',
        dest='config',
        type=str,
        required=True,
    )
    return parser.parse_args()


def main():
    args = get_args()
    parser = ConfigParser()
    parser.read(args.config, encoding='utf-8')
    try:
        username = parser.get('DEFAULT', 'username')
        password = parser.get('DEFAULT', 'password')
        port = parser.get('DEFAULT', 'port')
        command = getenv('IPMI_COMMAND', 'SMCIPMITool')
        port = int(port)
    except NoOptionError as e:
        stderr.write(f'Error: {e}\n')
        exit(1)
    except ValueError:
        stderr.write(f"Invalid value for option 'port': {port}\n")
        exit(1)

    logger = logging.getLogger(__name__)
    logger.info(f'Listening on port: {port}')

    def setup_handler(*args, **kwargs):
        return IPMICollectorHandler(
            command, username, password, *args, **kwargs
        )

    with ThreadingHTTPServer(('', port), setup_handler) as server:
        server.serve_forever()


if __name__ == '__main__':
    main()
