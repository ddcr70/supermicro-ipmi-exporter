import re
import shutil
import logging
from pathlib import Path
from subprocess import Popen, PIPE

logger = logging.getLogger(__name__)


RE_GET_TEMP = re.compile(r'(\d+\.?\d*)C')
RE_GET_VOLTS = re.compile(r'(\d+\.?\d*) V')
RE_GET_FAN = re.compile(r'(\d+\.?\d*) RPM')
RE_GET_POWER = re.compile(r'(\d+\.?\d*) W')
RE_GET_CURRENT = re.compile(r'(\d+\.?\d*) A')
RE_GET_PS_STATUS = re.compile(r'\(([0-9a-f]+)h\)')


class Result:
    __slots__ = ('name', 'help', 'value')

    def __init__(self, *, name='', help='', value=None):
        self.name = name
        self.help = help
        self.value = value

    def __repr__(self):
        return f'Result({self.name!r}, {self.help!r}, {self.value!r})'

    __str__ = __repr__


class Exporter:
    '''args: command, host, username, password
    '''

    __slots__ = 'command'

    def __init__(self, *args):
        self.command = args

    def read(*args):
        if (not Path(args[0]).exists()) and shutil.which(args[0]) is None:
            if not Path(args[0]).exists():
                logger.critical(f'File: {args[0]} does not exist')
            else:
                logger.critical(f'Command: {args[0]} not in $PATH')
            return None
        with Popen(
            args, stdout=PIPE, stderr=PIPE, cwd=Path(args[0]).parent
        ) as proc:
            out, err = proc.communicate()
            if err:
                logger.critical(f'Command failed: {err.decode("utf-8")}')
                return None
            return out.decode('utf-8')

    def power_status(self):
        logger.info('Collecting power status')
        result = Result(
            help='1 - on, 0 - off, -1 - failed', name='power_status'
        )
        subcommand = 'ipmi power status'.split()
        status = Exporter.read(*self.command, *subcommand)
        if status is None:
            result.value = -1
            return (result,)

        if status.strip() == 'Power is currently on.':
            value = 1
        elif status.strip() == 'Power is currently off.':
            value = 0
        else:
            value = -1

        result.value = value
        return (result,)

    def sensors(self):
        logger.info('Collecting sensor metrics')
        result = Result(help='see help for impi sensor subcommand')
        subcommand = 'ipmi sensor'.split()
        sensors = Exporter.read(*self.command, *subcommand)

        # Report command success
        result.name = 'motherboard_command_success'
        if sensors is None:
            result.value = 0
            return (result,)

        result.value = 1

        # Parse output
        results = [result]
        for i, line in enumerate(sensors.strip().split('\n')):
            # Skip first 3 rows
            if i < 3:
                continue
            result = Result()
            key, val = line.split('|')[1:3]
            key, val = key.strip(), val.strip()

            # Parse key
            key = re.sub(r'\(\d+\) ', '', key).lower()
            key = key.translate(
                {ord(' '): '_', ord('+'): '', ord('.'): '_', ord('-'): '_'}
            )
            result.name = f'motherboard_{key}'

            # Parse value
            match = RE_GET_TEMP.match(val)
            if match is not None:
                result.value = float(match.groups()[0])

            match = RE_GET_VOLTS.match(val)
            if match is not None:
                result.value = float(match.groups()[0])

            match = RE_GET_FAN.match(val)
            if match is not None:
                result.value = float(match.groups()[0])
            elif val == 'N/A':
                result.value = -1
            elif val == 'Low':
                result.value = 0
            elif val == 'Hi':
                result.value = 1

            if result.value is None:
                continue
            results.append(result)
        return results

    def pminfo(self):
        logger.info('Collecting power supply metrics')
        subcommand = ('pminfo',)
        power_supplies = Exporter.read(*self.command, *subcommand)
        result = Result(help='see help for pminfo subcommand')

        # Report command success
        result.name = 'ps_command_success'
        if power_supplies is None:
            result.value = 0
            return (result,)
        result.value = 1

        # Parse output
        results = [result]
        for j, power_supply in enumerate(
            power_supplies.strip().split('\n\n'), start=1
        ):
            for i, line in enumerate(power_supply.strip().split('\n')):
                # Skip first 3 rows
                if i < 3:
                    continue
                result = Result()
                key, val = line.split('|')
                key, val = key.strip(), val.strip()
                result.name = f"ps_{key.lower().replace(' ', '_')}_module_{j}"

                # Parse value
                match = RE_GET_POWER.match(val)
                if match is not None:
                    result.value = float(match.groups()[0])

                match = RE_GET_CURRENT.match(val)
                if match is not None:
                    result.value = float(match.groups()[0])

                match = RE_GET_FAN.match(val)
                if match is not None:
                    result.value = float(match.groups()[0])

                match = RE_GET_PS_STATUS.match(val)
                if match is not None:
                    result.value = int(match.groups()[0], base=16)
                    result.help += 'hours active'

                if result.value is None:
                    continue
                results.append(result)
        return results
